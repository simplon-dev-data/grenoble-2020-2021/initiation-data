# Initiation Data

> Découvrir le processus des traitements de données numériques,
> en réalisant une cartographie interactive des points d'apport
> volontaires de la métropole de Grenoble

Le notebook Jupyter [cartographie_pav_session.ipynb](cartographie_pav_session.ipynb)
contient l'énoncé et l'activité.

Le notebook Jupyter [cartographie_pav.ipynb](cartographie_pav.ipynb)
contient l'énoncé, l'activité et la solution.

## Utilisation

1. `pipenv install -d`
2. `pipenv shell`
3. `jupyter notebook`

## Groupes

### De 14h à 15h : parties importation et nettoyage des données

HAMIDA, FRANCOIS (BOTTE), GABRIEL, ANTHONY, MEROUANE, THEO, HELENA, CINTHYA, MONCEF

**Suggestion de plan de l'activité :**

- Questions sur le niveau de connaissances en programmation et Python

- Questions sur le niveau de connaissances sur les données (tableurs, place des données...)

- Présentation des étapes de traitements de la donnée

- Présentation de l'activité dans sa globalité

- Présentation de la source de données

- Activité

- Bilan / conclusion / questions

**Sujets à aborter :**

- Langage de programmation / Python

- Notebooks / Jupyter

- Sources de données / Opendata

- Formats de données / Fichiers plats, bases de données

- Bibliothèques Python

- Fonctions / méthodes, variables en Python

- Qualité des données

---------------

### De 15h à 16h : partie visualisation des données (cartographie)

ARNAUD, FRANCOIS (OKOLO), MALIKA, XAVIER, ASMAA, THIENVU, MAXIME, KAMEL, RAYANE

**Suggestion de plan de l'activité :**

- Point rapide sur le niveau de programmation et compréhension de la première partie

- Récapitulatif de la première partie

- Objectif de la visualisation

- Données de géolocalisation

- Utilité de la cartographie

- Activité

- Bilan / conclusion / questions

**Sujets à aborter :**

- Visualisations de cartographie

- Données de géolocalisation

- Autres types de visualisations

- Impacts / intérêt de la visualisation
